#include "ofApp.h"
#include <math.h>
#include <fstream>

//--------------------------------------------------------------
void ofApp::setup(){
    
    // :::::::::: oF BASIC SETUP ::::::::::::::::::::::::::::::::
    ofSetVerticalSync(true);  // synchlonise the frame rate with the actual drawing rate
    ofEnableNormalizedTexCoords();
    glEnable(GL_POINT_SMOOTH); // use circular points instead of square points
    glPointSize(3); // make the points bigger
    
    
    
    // :::::::::: MOTION SENSOR SETUP ::::::::::::::::::::::::::::
    ofxAccelerometer.setup(); // this initializes the accelerometer
    motionManager = [[CMMotionManager alloc] init];
    [motionManager startGyroUpdates];
    [motionManager startDeviceMotionUpdates];

    
    
    // :::::::::: VIDEO SETUP ::::::::::::::::::::::::::::::::::::
    videoPlayer.loadMovie("./Shibuya/R0010072.mp4");
//    videoPlayer.loadMovie("./Home/video360.mp4");
    videoPlayer.setVolume(0.0);
    selector = 1;
    videoPlayer.setLoopState(OF_LOOP_NORMAL);

    
    
    // :::::::::: VIRTUAL SPACE SETUP ::::::::::::::::::::::::::::
    //---------------------------------
    float r = 10.0f; // radius
    initial_lookdir = ofVec3f(-r,0,0);
    initial_updir = ofVec3f(0,1,0);
    initial_hordir = initial_lookdir*initial_updir;
    //---------------------------------
    
    cam.setAutoDistance(false);
    cam.setDistance(0);
    cam.setPosition(0,0,0);
    cam.lookAt(initial_lookdir,initial_updir);   // <== look at (-10,0,0) , the upper direction is (0,1,0)
    
    
    
    // :::::::::: OpenAL SETUP :::::::::::::::::::::::::::::::::::
    // fetch the info about the device
    device = alcOpenDevice(NULL);
    
    // create context and assign it as alcMakeContextCurrent
    // "context" means an internal state, and all the operations of OpenAL are applied to it
    if (device) {
        ALCcontext *context = alcCreateContext(device, NULL);
        alcMakeContextCurrent(context);
    }
    
    alcMacOSXRenderingQualityProcPtr RenderingQuality = (alcMacOSXRenderingQualityProcPtr)alGetProcAddress("alcMacOSXRenderingQuality");
    
    RenderingQuality(ALC_IPHONE_SPATIAL_RENDERING_QUALITY_HEADPHONES);
 
    // make a buffer in which a sound file will be stored
    ALuint buffer1, buffer2, buffer3, buffer4;
    alGetError();
    alGenBuffers(1, &buffer1);
    alGenBuffers(1, &buffer2);
    alGenBuffers(1, &buffer3);
    alGenBuffers(1, &buffer4);
    
    // read a sound file and put it in the buffer
    void *data1, *data2, *data3, *data4;
    ALenum format1, format2, format3, format4;
    ALsizei size1, size2, size3, size4;
    ALsizei freq1, freq2, freq3, freq4;
    
    string tr1pathStr = ofToDataPath("./Shibuya/ZOOM0011_Tr1.WAV");
    string tr2pathStr = ofToDataPath("./Shibuya/ZOOM0011_Tr2.WAV");
    string tr3pathStr = ofToDataPath("./Shibuya/ZOOM0011_Tr3.WAV");
    string tr4pathStr = ofToDataPath("./Shibuya/ZOOM0011_Tr4.WAV");
    
//    string tr1pathStr = ofToDataPath("./Home/channel_Tr1.wav");
//    string tr2pathStr = ofToDataPath("./Home/channel_Tr2.wav");
//    string tr3pathStr = ofToDataPath("./Home/channel_Tr3.wav");
//    string tr4pathStr = ofToDataPath("./Home/channel_Tr4.wav");
    
    NSString *path1 = [NSString stringWithCString:tr1pathStr.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    NSString *path2 = [NSString stringWithCString:tr2pathStr.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    NSString *path3 = [NSString stringWithCString:tr3pathStr.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    NSString *path4 = [NSString stringWithCString:tr4pathStr.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    
    CFURLRef fileURL1 = (__bridge CFURLRef)[NSURL fileURLWithPath:path1];
    CFURLRef fileURL2 = (__bridge CFURLRef)[NSURL fileURLWithPath:path2];
    CFURLRef fileURL3 = (__bridge CFURLRef)[NSURL fileURLWithPath:path3];
    CFURLRef fileURL4 = (__bridge CFURLRef)[NSURL fileURLWithPath:path4];
    
    data1 = MyGetOpenALAudioData(fileURL1, &size1, &format1, &freq1);
    data2 = MyGetOpenALAudioData(fileURL2, &size2, &format2, &freq2);
    data3 = MyGetOpenALAudioData(fileURL3, &size3, &format3, &freq3);
    data4 = MyGetOpenALAudioData(fileURL4, &size4, &format4, &freq4);
    
    alBufferDataStaticProc(buffer1, format1, data1, size1, freq1);
    alBufferDataStaticProc(buffer2, format2, data2, size2, freq2);
    alBufferDataStaticProc(buffer3, format3, data3, size3, freq3);
    alBufferDataStaticProc(buffer4, format4, data4, size4, freq4);
    
    
    // make a sound source
    alGetError();
    alGenSources(1, &tr1);
    alGenSources(1, &tr2);
    alGenSources(1, &tr3);
    alGenSources(1, &tr4);
    
    // bind the buffer to the sound source
    alSourcei(tr1, AL_BUFFER, buffer1);
    alSourcei(tr2, AL_BUFFER, buffer2);
    alSourcei(tr3, AL_BUFFER, buffer3);
    alSourcei(tr4, AL_BUFFER, buffer4);
    
    // source setting
    float dist = 1;
    ALfloat tr1_pos[] = {dist*cos(PI/3.0),0,dist*sin(PI/3.0)};
    ALfloat tr2_pos[] = {-dist*cos(PI/3.0),0,dist*sin(PI/3.0)};
    ALfloat tr3_pos[] = {0,-dist*cos(PI/3.0),-dist*sin(PI/3.0)};
    ALfloat tr4_pos[] = {0,dist*cos(PI/3.0),-dist*sin(PI/3.0)};
    
    alSourcefv(tr1, AL_POSITION, tr1_pos);
    alSourcefv(tr2, AL_POSITION, tr2_pos);
    alSourcefv(tr3, AL_POSITION, tr3_pos);
    alSourcefv(tr4, AL_POSITION, tr4_pos);
    alSourcef(tr1, AL_REFERENCE_DISTANCE, 1.0f);
    alSourcef(tr2, AL_REFERENCE_DISTANCE, 1.0f);
    alSourcef(tr3, AL_REFERENCE_DISTANCE, 1.0f);
    alSourcef(tr4, AL_REFERENCE_DISTANCE, 1.0f);

    // additional adjustments
    alSourcei(tr1, AL_LOOPING, AL_TRUE);
    alSourcei(tr2, AL_LOOPING, AL_TRUE);
    alSourcei(tr3, AL_LOOPING, AL_TRUE);
    alSourcei(tr4, AL_LOOPING, AL_TRUE);
    alSourcef(tr1, AL_GAIN, 6.0f);
    alSourcef(tr2, AL_GAIN, 6.0f);
    alSourcef(tr3, AL_GAIN, 6.0f);
    alSourcef(tr4, AL_GAIN, 6.0f);
    
    
    
    
    // listener property
    ALfloat listener_pos[] = {0.0, 0.0, 0.0};
    ALfloat listener_ori[] = {initial_lookdir.x, initial_lookdir.y, initial_lookdir.z, initial_hordir.x, initial_hordir.y, initial_hordir.z};
    alListenerfv(AL_POSITION, listener_pos);
    alListenerfv(AL_ORIENTATION, listener_ori);
    
    
    
    // :::::::::: START PROGRAM ::::::::::
    videoPlayer.play();
    alSourcePlay(tr1);
    alSourcePlay(tr2);
    alSourcePlay(tr3);
    alSourcePlay(tr4);

}

//--------------------------------------------------------------
void ofApp::update(){
    videoPlayer.update();
    if (videoPlayer.isFrameNew()) {
        texture = videoPlayer.getTextureReference();
    }
    
    float up_down = motionManager.deviceMotion.attitude.roll * RAD_TO_DEG -90;
    float right_left = motionManager.deviceMotion.attitude.yaw * RAD_TO_DEG;
    
    lookdir = initial_lookdir;
    updir = initial_updir;
    hordir = updir.getCrossed(lookdir); // <== updir * lookddir (cross product)
    
    /* Turning RIGHT or LEFT */
    lookdir.rotate(right_left, hordir);
    updir.rotate(right_left, hordir);
    
    /* Turning UPWARD or DOWNWARD */
    lookdir.rotate(up_down, updir).normalize();
    hordir.rotate(up_down, updir).normalize();
    
    cam.lookAt(lookdir, updir);
    ALfloat listener_ori[] = {lookdir.x, lookdir.y, lookdir.z, hordir.x, hordir.y, hordir.z};
    alListenerfv(AL_ORIENTATION, listener_ori);
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackgroundGradient(ofColor::gray, ofColor::black, OF_GRADIENT_CIRCULAR);
    
    cam.begin();
    ofPushMatrix();
    ofMatrix4x4 rot(0,-1,0,0,
                    0,0,-1,0,
                    1,0,0,0,
                    0,0,0,1);
    ofMultMatrix(rot);
    texture.bind();
    ofDrawSphere(7);
    texture.unbind();
    ofPopMatrix();
    cam.end();

}

//--------------------------------------------------------------
void ofApp::exit(){

}

//--------------------------------------------------------------
void ofApp::touchDown(ofTouchEventArgs & touch){
    cout << "touchDown" << endl;
}

//--------------------------------------------------------------
void ofApp::touchMoved(ofTouchEventArgs & touch){
    cout << "touchMoved" << endl;
}

//--------------------------------------------------------------
void ofApp::touchUp(ofTouchEventArgs & touch){
    cout << "touchUp" << endl;
}

//--------------------------------------------------------------
void ofApp::touchDoubleTap(ofTouchEventArgs & touch){
    cout << "touchDoubleTap" << endl;
    
//    if (selector == 1) {
//        videoPlayer.setVolume(3.0);
//        alSourcef(tr1, AL_GAIN, -INFINITY);
//        alSourcef(tr2, AL_GAIN, -INFINITY);
//        alSourcef(tr3, AL_GAIN, -INFINITY);
//        alSourcef(tr4, AL_GAIN, -INFINITY);
//        selector = 0;
//    }
//    else if (selector == 0) {
//        videoPlayer.setVolume(0.0);
//        alSourcef(tr1, AL_GAIN, 3.0f);
//        alSourcef(tr2, AL_GAIN, 3.0f);
//        alSourcef(tr3, AL_GAIN, 3.0f);
//        alSourcef(tr4, AL_GAIN, 3.0f);
//        selector = 1;
//    }
}

//--------------------------------------------------------------
void ofApp::touchCancelled(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void ofApp::lostFocus(){

}

//--------------------------------------------------------------
void ofApp::gotFocus(){

}

//--------------------------------------------------------------
void ofApp::gotMemoryWarning(){

}

//--------------------------------------------------------------
void ofApp::deviceOrientationChanged(int newOrientation){

}

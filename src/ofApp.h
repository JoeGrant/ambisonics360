#pragma once

#include "ofMain.h"
#include "ofxiOS.h"
#include "ofxiOSExtras.h"
#include "oalMacOSX_OALExtensions.h"
#include <UIKit/UIKit.h>
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#include <AudioToolbox/AudioToolbox.h>
#include <CoreMotion/CoreMotion.h>

extern"C" {
    #include"MyOpenALSupport.h"
};


class ofApp : public ofxiOSApp {
	
    public:
    
    
    
        void setup();
        void update();
        void draw();
        void exit();
	
        void touchDown(ofTouchEventArgs & touch);
        void touchMoved(ofTouchEventArgs & touch);
        void touchUp(ofTouchEventArgs & touch);
        void touchDoubleTap(ofTouchEventArgs & touch);
        void touchCancelled(ofTouchEventArgs & touch);

        void lostFocus();
        void gotFocus();
        void gotMemoryWarning();
        void deviceOrientationChanged(int newOrientation);
    
    CMMotionManager *motionManager;
    ofVideoGrabber videoGrabber;
    ofVideoPlayer videoPlayer;
    
    ofEasyCam cam;
    ofVboMesh mesh;
    ofImage img;
    ofTexture texture;
    
    std::string filename;
    float compass, zenith_x, zenith_y;
    
    ofVec3f lookdir, updir, hordir;
    
    ofVec3f initial_lookdir, initial_updir, initial_hordir;

    ALCdevice *device;
    ALuint tr1, tr2, tr3, tr4;

    int selector;

};